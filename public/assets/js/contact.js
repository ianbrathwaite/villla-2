const form = document.getElementById('contact-form')
const name = document.getElementById('name').value
const message = document.getElementById('message').value
const email = document.getElementById('email').value

form.addEventListener('submit', function (e) {
  e.preventDefault()
  e.stopImmediatePropagation()

  const xhr = new XMLHttpRequest()
  xhr.open('POST', '/contact')
  xhr.setRequestHeader('Content-Type', 'application/json')
  xhr.onload = function() {
    if (xhr.status === 200) {
      success.style.opacity = 1 // toggle flash
      error.style.opacity = 0
    } else {
      error.style.opacity = 0
      success.style.opacity = 0
      setTimeout(function() {
        error.style.opacity = 1
      }, 250)
    }
  }
  xhr.send(JSON.stringify({
    name: name,
    email: email,
    message: message
  }))
})
